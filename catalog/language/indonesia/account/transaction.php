<?php
/*
// Heading 
$_['heading_title']      = 'Your Transactions';

// Column
$_['column_date_added']  = 'Date Added';
$_['column_description'] = 'Description';
$_['column_amount']      = 'Amount (%s)';

// Text
$_['text_account']       = 'Account';
$_['text_transaction']   = 'Your Transactions';
$_['text_total']         = 'Your current balance is:';
$_['text_empty']         = 'You do not have any transactions!';
*/

// Heading
$_['heading_title'] 	    = 'Transaksi Anda;

// Kolom
$_['column_date_added']  = 'Tanggal Ditambahkan';
$_['column_description'] = 'Deskripsi';
$_['column_amount'] 	    = 'Jumlah (% s)';

// Teks
$_['text_account'] 	    = 'Akun';
$_['text_transaction']   = 'Transaksi Anda;
$_['text_total'] 	    = 'Saldo anda saat ini adalah:';
$_['text_empty'] 	    = 'Anda tidak memiliki transaksi!';
?>