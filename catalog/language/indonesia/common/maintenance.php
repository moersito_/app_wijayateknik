<?php
// Heading
$_['heading_title']    = 'Perawatan';

// Text
$_['text_maintenance'] = 'Perawatan';
$_['text_message']     = '<h1 style="text-align:center;">Kami sedang melakukan beberapa pemeliharaan terjadwal. <br/>Kami akan kembali secepatnya. Silakan segera periksa kembali.</h1>';
?>