<?php
// Locale
$_['code']                  = 'id';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = 'Beranda';
$_['text_yes']              = 'Ya';
$_['text_no']               = 'Tidak';
$_['text_none']             = ' --- Tidak Satupun --- ';
$_['text_select']           = ' --- Silahkan Pilih --- ';
$_['text_all_zones']        = 'Semua Zona';
$_['text_pagination']       = 'Menampilkan {start} ke {end} dari {total} ({pages} Halaman)';
$_['text_separator']        = ' &raquo; ';

// Buttons
$_['button_add_address']    = 'Tambah Alamat';
$_['button_back']           = 'Kembali';
$_['button_continue']       = 'Lanjutkan';
$_['button_cart']           = 'Beli';
$_['button_compare']        = 'Bandingkan';
$_['button_wishlist']       = 'Tambah ke Daftar Keinginan';
$_['button_checkout']       = 'Periksa';
$_['button_confirm']        = 'Konfirmasi Pesanan';
$_['button_coupon']         = 'Terapkan Kupon';
$_['button_delete']         = 'Hapus';
$_['button_download']       = 'Unduh';
$_['button_edit']           = 'Ubah';
$_['button_filter']         = 'Perbaiki Pencarian';
$_['button_new_address']    = 'Alamat Baru';
$_['button_change_address'] = 'Ubah Alamat';
$_['button_reviews']        = 'Ulasan';
$_['button_write']          = 'Tulis Ulasan';
$_['button_login']          = 'Masuk';
$_['button_update']         = 'Perbarui';
$_['button_remove']         = 'Hapus';
$_['button_reorder']        = 'Pesan Kembali';
$_['button_return']         = 'Kembali';
$_['button_shopping']       = 'Lanjutkan Berbelanja';
$_['button_search']         = 'Cari';
$_['button_shipping']       = 'Terapkan Pengiriman';
$_['button_guest']          = 'Periksa Tamu';
$_['button_view']           = 'Lihat';
$_['button_voucher']        = 'Terapkan Voucher';
$_['button_upload']         = 'Unggah File';
$_['button_reward']         = 'Terapkan Point';
$_['button_quote']          = 'Dapatkan Tanda Kutip';

// Error
$_['error_upload_1']        = 'Peringatan: Berkas yang diunggah melebihi direktif upload_max_filesize dalam php.ini!';
$_['error_upload_2']        = 'Peringatan: Berkas yang diunggah melampaui direktif MAX_FILE_SIZE yang ditentukan dalam bentuk HTML!';
$_['error_upload_3']        = 'Peringatan: Berkas yang diunggah hanya berhasil diunggah sebagian!';
$_['error_upload_4']        = 'Peringatan: Tidak ada file yang diupload!';
$_['error_upload_6']        = 'Peringatan: Hilang folder sementara!';
$_['error_upload_7']        = 'Peringatan: Gagal menulis file ke disk!';
$_['error_upload_8']        = 'Peringatan: File upload dihentikan oleh ekstensi!';
$_['error_upload_999']      = 'Peringatan: Tidak ada kode kesalahan tersedia!';
?>