<?php
/*
// Heading
$_['heading_title']     = 'Product Comparison';
 
// Text
$_['text_product']      = 'Product Details';
$_['text_name']         = 'Product';
$_['text_image']        = 'Image';
$_['text_price']        = 'Price';
$_['text_model']        = 'Model';
$_['text_manufacturer'] = 'Brand';
$_['text_availability'] = 'Availability';
$_['text_instock']      = 'In Stock';
$_['text_rating']       = 'Rating';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_summary']      = 'Summary';
$_['text_weight']       = 'Weight';
$_['text_dimension']    = 'Dimensions (L x W x H)';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_success']      = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">product comparison</a>!';
$_['text_remove']       = 'Success: You have modified your product comparison!';
$_['text_empty']        = 'You have not chosen any products to compare.';
*/
// Heading
$_['heading_title']= 'Perbandingan Produk';
 
// Teks
$_['text_product'] = 'Detail Produk';
$_['text_name']    = 'Produk';
$_['text_image']   = 'Gambar';
$_['text_price']   = 'Harga';
$_['text_model']   = 'Model';
$_['text_manufacturer'] = 'Merek';
$_['text_availability'] = 'Ketersediaan';
$_['text_instock'] = 'Stok';
$_['text_rating']  = 'Penilaian';
$_['text_reviews'] = 'Berdasarkan ulasan% s.';
$_['text_summary'] = 'Ringkasan';
$_['text_weight']  = 'Berat';
$_['text_dimension'] = 'Dimensi (P x L x H)';
$_['text_compare'] = 'Produk Bandingkan (% s)';
$_['text_success'] = 'Sukses: Anda telah menambahkan <a href="%s">% s </ a> untuk href="%s"> <a produk perbandingan </ a>';
$_['text_remove']  = 'Sukses: Anda telah memodifikasi produk perbandingan Anda';
$_['text_empty']   = 'Anda belum memilih produk-produk untuk membandingkan.';
?>