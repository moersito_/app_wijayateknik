<?php
// Heading
$_['heading_title']     = 'Ekspedisi';

// Text
$_['text_install']      = 'Install';
$_['text_uninstall']    = 'Uninstall';

// Column
$_['column_name']       = 'Metode Ekspedisi';
$_['column_status']     = 'Status';
$_['column_sort_order'] = 'Urutan';
$_['column_action']     = 'Tindakan';

// Error
$_['error_permission']  = 'Warning: Anda tidak punya izin mengubah ekspedisi!';
?>