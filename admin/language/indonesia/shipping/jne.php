<?php
// Heading
$_['heading_title']         = 'JNE Reguler Indonesia';

// Text
$_['text_shipping']         = 'Ekspedisi';
$_['text_success']          = 'Sukses: Anda telah memodifikasi pengaturan ekspedisi!';
$_['text_no_results']       = 'Tidak Ada Hasil'; 
$_['text_select_province']  = 'Pilih Provinsi';
$_['text_legend_primary']   = 'Data Primer';
$_['text_legend_secondary'] = 'Data Sekunder';

// Button
#
$_['button_add_rates']      = 'Tambah';
$_['button_cancel_addrates']= 'Batal';
$_['button_edit_rates']     = 'Ubah'; 
$_['button_remove']         = 'Hapus';
#
// Entry
#
$_['entry_cost_code']       = 'Kode Provinsi';
$_['entry_province']        = 'Provinsi';
$_['entry_cost_city']       = 'Kota Tujuan';
$_['entry_cost_price']      = 'Harga/Biaya';
$_['entry_cost_weight']     = 'Berat (Kg)';
#
$_['entry_hometown']        = 'Kota Asal';
$_['entry_cost']            = 'Tarif';
$_['entry_tax']             = 'Kelas Pajak';
$_['entry_geo_zone']        = 'Zona';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Urutan';

// Fieldname
#
#$_['col_rates_code']   = 'Kode';
#$_['col_rates_city']   = 'Kota Tujuan';
#$_['col_rates_price']  = 'Ongkos/Tarif';
$_['col_rates_date']        = 'Tanggal Ubah'; 
$_['col_rates_act']         = 'Tindakan'; 
#
// Error
$_['error_permission']      = 'Peringatan: Anda tidak punya izin untuk memodifikasi pengaturan pengiriman!';
?>