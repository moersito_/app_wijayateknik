<?php 
class ModelSettingSetting extends Model 
{
	public function getSetting($group, $store_id = 0) {
		$data = array(); 
		
		$query= $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
		
		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = unserialize($result['value']);
			}
		}
		return $data;
	}
	
	public function editSetting($group, $data, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");

		foreach ($data as $key => $value) {
			if (!is_array($value)) {
				$this->db->query("INSERT INTO ". DB_PREFIX ."setting 
				                  SET store_id = '". (int)$store_id ."', 
				                     `group`   = '". $this->db->escape($group) ."', 
				                     `key`     = '". $this->db->escape($key) ."', 
				                     `value`   = '". $this->db->escape($value) ."'");
			} else {
				$this->db->query("INSERT INTO ". DB_PREFIX ."setting 
				                  SET store_id = '". (int)$store_id ."', 
				                     `group`   = '". $this->db->escape($group) ."', 
				                     `key`     = '". $this->db->escape($key) ."', 
				                     `value`   = '". $this->db->escape(serialize($value)) ."', 
				                      serialized = '1'");
			}
		}
	}
	
	public function deleteSetting($group, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
	}
	
	public function editSettingValue($group = '', $key = '', $value = '', $store_id = 0) {
		if (!is_array($value)) {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "' WHERE `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape(serialize($value)) . "' WHERE `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "', serialized = '1'");
		}
	}
	
	/**********************************************************
     *     METHOD's TAMBAHAN - CREATE BY GREENHOLE.WEB.ID     *
     **********************************************************/
    
    protected function arraysChecking($data, $delimiter='') {
        $delimiter = empty($delimiter) ? ',' : $delimiter;
        return !is_array($data) ? explode($delimiter, $data) : $data;
    } 
    
    protected function arraysForQuery($array) {
        $pieces = array();
        foreach($array as $ii => $el){
           $pieces[$ii] = "'". $this->db->escape($el) ."'";  
        }
        return implode(',', $pieces);
    }
    
    public function sqlConfShipping($selecting='',$method='') {
        $selecting = empty($selecting) ? '*' : $selecting;
        $method  = empty($method) ? 'jne' : '';
        $queries = "SELECT {$selecting} FROM ". DB_PREFIX ."zz_extra_shippingconf 
                    WHERE status = 1 AND method = '{$method}' 
                    ORDER BY province_code ASC, cityname ASC";
        return $queries;            
    }
     
    public function getDetailsConfShipping($data=array()) {
        #[sql statement] 
        $queries = $this->sqlConfShipping();      
        #[control]            
        if(isset($data['start']) || isset($data['limit'])) {
              if($data['start'] < 0) $data['start'] = 0;
              if($data['limit'] < 1) $data['limit'] = 4;
              #[adding query] 
              $queries .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }                       
        #[execution] 
        $equal = $this->db->query($queries);                   
        return $equal->rows;                         
    } 
    
    public function getTotalConfShipping() {
        #[sql statement]           
        $queries = $this->sqlConfShipping('COUNT(*) AS `total`');            
        #[execution]  
        $equal = $this->db->query($queries);           
        return $equal->row['total'];                    
    }
    
    public function updateCostShippingToSetting($group='',$fieldkey='') {
        $group = empty($group) ? 'jne' : $group;
        $fieldkey = empty($fieldkey) ? 'jne_cost' : $fieldkey; 
        #[read table]
        $read_shipping = $this->db->query($this->sqlConfShipping('province_code,cityname,rates'));
        $temp = array();
        foreach($read_shipping->rows as $pieces) {
           $temp[] = implode(',',$pieces); 
        }
        $data_cost = implode('\r\n',$temp); 
        #[update table]
        $queries = "UPDATE ". DB_PREFIX ."setting SET `value` = '{$data_cost}' 
                    WHERE `key` = '{$fieldkey}' AND `group` = '{$group}'"; 
        $this->db->query($queries);               
    }
     
    public function saveConfShipping($data,$method='') {
        #[assignment]
        $method = empty($method) ? 'jne' : $method;
        $data   = $this->arraysChecking($data);  
        $exts_data = array('shipping_status'=>'1','shipping_method'=>$method,'shipping_date_added'=>date('Y-m-d H:i:s')); 
        $real_data = array_merge($data, $exts_data);     
        $insr_data = $this->arraysForQuery($real_data);
        #[sql statement]
        $queries = "INSERT INTO ". DB_PREFIX ."zz_extra_shippingconf
                    (province_code, cityname, rates, status, method, date_added) 
                    VALUES ({$insr_data})";
        #[execution]
        $this->db->query($queries);
        #[confirmed & updated]
        if($this->db->countAffected() > 0) {
           $this->updateCostShippingToSetting(); 
        }
    }
    
    public function deleteConfShipping($data,$method='') {
        #[assignment]
        $method = empty($method) ? 'jne' : $method;
        #[execution]
        foreach($data as $el) {
         $split = explode('|', $el);    
         $this->db->query("DELETE FROM ". DB_PREFIX ."zz_extra_shippingconf 
                           WHERE province_code='". $split[0] ."' AND cityname='". $split[1] ."' AND method='{$method}'");
        }                            
        #[confirmed & updated]
        if($this->db->countAffected() > 0) {
           $this->updateCostShippingToSetting(); 
        }                  
    } 	
    
    public function editConfShipping($data,$method='') {
        $method = empty($method) ? 'jne' : $method;
        $tablename = DB_PREFIX ."zz_extra_shippingconf";
        #[proses hapus]
        $this->db->query("DELETE FROM {$tablename} 
                          WHERE province_code = '". $data['old_shipping_code'] ."' 
                          AND cityname        = '". $data['old_shipping_city'] ."'
                          AND rates           = '". $data['old_shipping_rate'] ."'
                          AND method          = '". $method ."'");
        #[proses insert pengganti update]
        $this->db->query("INSERT INTO {$tablename} (province_code,cityname,rates,status,method,date_added)
                          VALUES ('". $data['shipping_code'] ."','". $data['shipping_city'] ."','". $data['shipping_rate'] ."','1','". $method ."',NOW())");
        #[confirmed & updated]
        if($this->db->countAffected() > 0) {
           $this->updateCostShippingToSetting(); 
        }                          
    } 
    
    public function getProvinceTags() {
        #[assignment] 
        $geo  = explode(';', $_SERVER['HTTP_COOKIE']); #-- __atuvc=2%7C3; language=id; currency=IDR; __zlcmid=MudSagfdMeRHf4; PHPSESSID=lq0mn8hpftt1ss54ibafjhnp46 
        $lang = explode('=', $geo[1]);
        $country_iso_code2 = strtoupper($lang[1]);
        #[sql statement]
        $queries = "SELECT a.zone_id, a.name, a.code 
                    FROM ". DB_PREFIX ."zone a, ". DB_PREFIX ."country b 
                    WHERE a.country_id  = b.country_id 
                    AND b.iso_code_2 = '{$country_iso_code2}'";
        #[execution]            
        return $this->db->query($queries)->rows;                    
    }
     
} // End: Class
?>