<?php
// ORIGIN ROUTE
define('BACKEND_DIR'     , basename(dirname(__FILE__)));

// HTTP
define('HTTP'		 , $_SERVER['HTTP_HOST'].str_replace('/'.BACKEND_DIR, '',dirname($_SERVER['PHP_SELF'])));
define('HTTP_SERVER'	 , 'http://'.HTTP.'/'.BACKEND_DIR.'/');
define('HTTP_CATALOG'    , 'http://'.HTTP.'/');
define('HTTP_IMAGE'	 , 'http://'.HTTP.'/image/');

// HTTPS
define('HTTPS_SERVER'    , 'http://'.HTTP.'/'.BACKEND_DIR.'/');
define('HTTPS_IMAGE'	 , 'http://'.HTTP.'/image/');

// DIR
define('BASE_DIR'	 , str_replace(DIRECTORY_SEPARATOR.BACKEND_DIR, '', realpath(dirname(__FILE__))));
define('DIR_APPLICATION' , BASE_DIR.'/'.BACKEND_DIR.'/');
define('DIR_SYSTEM'	 , BASE_DIR.'/system/');
define('DIR_DATABASE'    , BASE_DIR.'/system/database/');
define('DIR_LANGUAGE'    , BASE_DIR.'/'.BACKEND_DIR.'/language/');
define('DIR_TEMPLATE'    , BASE_DIR.'/'.BACKEND_DIR.'/view/template/');
define('DIR_CONFIG'	 , BASE_DIR.'/system/config/');
define('DIR_IMAGE'	 , BASE_DIR.'/image/');
define('DIR_CACHE'	 , BASE_DIR.'/system/cache/');
define('DIR_DOWNLOAD'    , BASE_DIR.'/download/');
define('DIR_LOGS'	 , BASE_DIR.'/system/logs/');
define('DIR_CATALOG'	 , BASE_DIR.'/catalog/'); 

// DB
include_once BASE_DIR.'/database/configuration.php';