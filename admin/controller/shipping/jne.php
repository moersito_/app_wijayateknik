<?php
class ControllerShippingjne extends Controller 
{
	private $error = array(); 
	
	public function index() 
	{   
		$this->load->language('shipping/jne');
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->load->model('setting/setting');
				
		if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		    
		    $form_category = array_shift($this->request->post);
            switch($form_category) {
                case 'primary':
                  $this->model_setting_setting->editSetting('jne',$this->request->post); //default API
                  $this->session->data['success'] = $this->language->get('text_success');
                  $this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));     
                  break;
                case 'secondary':
                  $this->model_setting_setting->saveConfShipping($this->request->post);
                  $this->session->data['success'] = $this->language->get('text_success');  
                  break;    
                case 'secondary-update':
                  $this->model_setting_setting->editConfShipping($this->request->post);
                  $this->session->data['success'] = $this->language->get('text_success');  
                  break;      
            } 
		}
        #[begin: additional by greenhole.web.id]
        #
        // Tabel Data Tarif Ekspedisi 
        $this->data['tarif_ekspedisi']       = $this->model_setting_setting->getDetailsConfShipping();
        $this->data['pagination']            = $this->paging();
        
        // Data Provinsi 
        $this->data['province_tags']         = $this->model_setting_setting->getProvinceTags();
        
        // Text
        $this->data['text_legend_primary']   = $this->language->get('text_legend_primary');
        $this->data['text_legend_secondary'] = $this->language->get('text_legend_secondary');
        $this->data['text_no_results']       = $this->language->get('text_no_results');
        $this->data['text_select_province']  = $this->language->get('text_select_province');
        // Button
        $this->data['button_add_rates']      = $this->language->get('button_add_rates');
        $this->data['button_cancel_addrates']= $this->language->get('button_cancel_addrates'); 
        $this->data['button_edit_rates']     = $this->language->get('button_edit_rates'); 
        $this->data['button_remove']         = $this->language->get('button_remove');
        // Input
        $this->data['entry_cost_code']       = $this->language->get('entry_cost_code');
        $this->data['entry_cost_city']       = $this->language->get('entry_cost_city');
        $this->data['entry_cost_price']      = $this->language->get('entry_cost_price');
        $this->data['entry_cost_weight']     = $this->language->get('entry_cost_weight');
        // Fieldname
        $this->data['col_rates_code']        = $this->language->get('col_rates_code');
        $this->data['col_rates_city']        = $this->language->get('col_rates_city');
        $this->data['col_rates_price']       = $this->language->get('col_rates_price');
        $this->data['col_rates_date']        = $this->language->get('col_rates_date'); 
        $this->data['col_rates_act']         = $this->language->get('col_rates_act'); 
        #[end]
        
        #[begin: native/legacy]
        // Heading
        $this->data['heading_title']         = $this->language->get('heading_title');
        // Text
        $this->data['text_enabled']          = $this->language->get('text_enabled');
        $this->data['text_disabled']         = $this->language->get('text_disabled');
        $this->data['text_all_zones']        = $this->language->get('text_all_zones');
        $this->data['text_none']             = $this->language->get('text_none');
        // Entry
        $this->data['entry_hometown']        = $this->language->get('entry_hometown');
        $this->data['entry_province']        = $this->language->get('entry_province');
	$this->data['entry_cost']            = $this->language->get('entry_cost');
	$this->data['entry_tax']             = $this->language->get('entry_tax');
	$this->data['entry_geo_zone']        = $this->language->get('entry_geo_zone');
	$this->data['entry_status']          = $this->language->get('entry_status');
	$this->data['entry_sort_order']      = $this->language->get('entry_sort_order');
        // Button
	$this->data['button_save']           = $this->language->get('button_save');
	$this->data['button_cancel']         = $this->language->get('button_cancel');
        // Tabbed
	$this->data['tab_general']           = $this->language->get('tab_general');
        #[end]
        
 		if(isset($this->error['warning'])) {
		   $this->data['error_warning'] = $this->error['warning'];
		}else{
		   $this->data['error_warning'] = '';
		}
  		$this->data['breadcrumbs']   = array();
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/jne', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
        #[create link button/form]
		$this->data['action'] = $this->url->link('shipping/jne', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/shipping' , 'token=' . $this->session->data['token'], 'SSL');
        $this->data['delete'] = $this->url->link('shipping/jne/delete', 'token=' . $this->session->data['token'], 'SSL');
         
        #[assigment & controlling] 
        $this->data['jne_cost']         = $this->inputControl('jne_cost');
        $this->data['jne_tax_class_id'] = $this->inputControl('jne_tax_class_id');
        $this->data['jne_geo_zone_id']  = $this->inputControl('jne_geo_zone_id');
        $this->data['jne_status']       = $this->inputControl('jne_status');
		$this->data['jne_sort_order']   = $this->inputControl('jne_sort_order');
        	
        #[get tax class from database then put into html holder]
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
        
		#[get geo zones from database then put into html holder]
		$this->load->model('localisation/geo_zone');
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        
		#[render view html]						
		$this->template = 'shipping/jne.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		$this->response->setOutput($this->render());
	}
	
    public function delete() {
        $this->load->model('setting/setting');
        
        if(empty($this->request->post['selected'])){
           $this->index(); 
        }else{
           $this->model_setting_setting->deleteConfShipping($this->request->post['selected']); 
           $this->redirect($this->url->link('shipping/jne', 'token=' . $this->session->data['token'], 'SSL'));
        }
    }
    
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/jne')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
        return !$this->error ? TRUE : FALSE; 
	}
    
    private function inputControl($dataname) {
        return isset($this->request->post[$dataname]) ? $this->request->post[$dataname] : $this->config->get($dataname);
    }
    
    public function paging() {
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        $data = array('start' => ($page - 1) * $this->config->get('config_admin_limit'),
                      'limit' => $this->config->get('config_admin_limit'),
        );
        $get_total_records = $this->model_setting_setting->getTotalConfShipping;
        $url = '';
        
        $pagination = new Pagination();
        $pagination->total = $get_total_records;
        $pagination->page  = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text  = $this->language->get('text_pagination');
        $pagination->url   = $this->url->link('shipping/jne', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
                    
        //$this->data['pagination'] = $pagination->render();
        return $pagination->render();  
    }
}
?>