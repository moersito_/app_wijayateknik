<?php
// Text
$_['text_subject']      = '%s ご注文状況のお知らせ 注文番号No: %s';
$_['text_order']        = 'ご注文番号:';
$_['text_date_added']   = 'ご注文日:';
$_['text_order_status'] = '現在のご注文状況:';
$_['text_comment']      = 'コメント:';
$_['text_invoice']      = 'ご注文についての情報を下記URLでご覧になれます。<br />:';
$_['text_footer']       = 'ご注文状況についてご質問等がございましたら、メールにて当店までお問い合わせくださいますよう、お願い申し上げます。';
?>