<?php
/**
 * @version		$Id: play.php 3399 2013-08-31 09:18:39Z mic $
 * @package		Translation German
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

$_['heading_title']         = 'Play.com (EU)';
$_['lang_heading_title']    = 'OpenBay Pro für Play.com';