<?php
/**
 * @version		$Id: amazon_checkout_layout.php 3399 2013-08-31 09:18:39Z mic $
 * @package		Translation German
 * @author		mic - http://osworx.net
 * @copyright	2013 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Heading
$_['heading_title']			= 'Amazon Bezahlungsbutton';

$_['text_module']			= 'Module';
$_['text_success']			= 'Datensatz erfolgreich aktualisiert!';
$_['text_content_top']		= 'Inhalt Oben';
$_['text_content_bottom']	= 'Inhalt Unten';
$_['text_column_left']		= 'Spalte Links';
$_['text_column_right']		= 'Spalte Rechts';

$_['entry_layout']			= 'Layout';
$_['entry_position']		= 'Position';
$_['entry_status']			= 'Status';
$_['entry_sort_order']		= 'Reihenfolge';

$_['error_permission']		= 'Hinweis: keine Rechte zur Bearbeitung dieses Moduls!';